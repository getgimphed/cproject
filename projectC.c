#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <stdio.h>
#include <stdint.h>
//#include <ctype.h>
#include <errno.h>
#define SOME_BIG_THING 512
#define TABSIZE 1600 /* 16M */

ssize_t getdelim(char **linep, size_t *n, int delim, FILE *fp);
ssize_t getline(char **linep, size_t *n, FILE *fp);


struct sim {
	uint64_t id;
	uint32_t dpc[4];
	uint32_t filler[56];
	};
	
int comp (const void * a, const void * b)
{
  return ( ((struct sim*)a)->id - ((struct sim*)b)->id );
}

int main(){
	FILE *fp;int i=0;
	ssize_t read;
	char * line = NULL;
	size_t len = 0;
	struct sim arr[SOME_BIG_THING];
	fp = fopen("input.txt", "r");
	if (fp == NULL)
		exit(EXIT_FAILURE);

	while((read = getline(&line, &len, fp)) != -1){
//		printf("%s", line);
		sscanf(line,"%d ,[%d ,%d ,%d ,%d ],[%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d \
		,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d, %d ,%d ,%d ,%d ,%d ,%d ,%d ,%d",&arr[i].id,&arr[i].dpc[0],&arr[i].dpc[1],&arr[i].dpc[2],&arr[i].dpc[3],\
		&arr[i].filler[0],&arr[i].filler[1],&arr[i].filler[2],&arr[i].filler[3],&arr[i].filler[4],&arr[i].filler[5],&arr[i].filler[6],&arr[i].filler[7],&arr[i].filler[8],&arr[i].filler[9],\
		&arr[i].filler[10],&arr[i].filler[11],&arr[i].filler[12],&arr[i].filler[13],&arr[i].filler[14],&arr[i].filler[15],&arr[i].filler[16],&arr[i].filler[17],&arr[i].filler[18],&arr[i].filler[19],\
		&arr[i].filler[20],&arr[i].filler[21],&arr[i].filler[22],&arr[i].filler[23],&arr[i].filler[24],&arr[i].filler[25],&arr[i].filler[26],&arr[i].filler[27],&arr[i].filler[28],&arr[i].filler[29],\
		&arr[i].filler[30],&arr[i].filler[31],&arr[i].filler[32],&arr[i].filler[33],&arr[i].filler[34],&arr[i].filler[35],&arr[i].filler[36],&arr[i].filler[37],&arr[i].filler[38],&arr[i].filler[39],\
		&arr[i].filler[40],&arr[i].filler[41],&arr[i].filler[42],&arr[i].filler[43],&arr[i].filler[44],&arr[i].filler[45],&arr[i].filler[46],&arr[i].filler[47],&arr[i].filler[48],&arr[i].filler[49],\
		&arr[i].filler[50],&arr[i].filler[51],&arr[i].filler[52],&arr[i].filler[53],&arr[i].filler[54],&arr[i].filler[55]);
//		printf("%d ,[%d ,%d ,%d ,%d ],[%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d \
//		,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d ,%d, %d ,%d ,%d ,%d ,%d ,%d ,%d ,%d]\n",arr[i].id,arr[i].dpc[0],arr[i].dpc[1],arr[i].dpc[2],arr[i].dpc[3],
//		arr[i].filler[0],arr[i].filler[1],arr[i].filler[2],arr[i].filler[3],arr[i].filler[4],arr[i].filler[5],arr[i].filler[6],arr[i].filler[7],arr[i].filler[8],arr[i].filler[9],
//		arr[i].filler[10],arr[i].filler[11],arr[i].filler[12],arr[i].filler[13],arr[i].filler[14],arr[i].filler[15],arr[i].filler[16],arr[i].filler[17],arr[i].filler[18],arr[i].filler[19],
//		arr[i].filler[20],arr[i].filler[21],arr[i].filler[22],arr[i].filler[23],arr[i].filler[24],arr[i].filler[25],arr[i].filler[26],arr[i].filler[27],arr[i].filler[28],arr[i].filler[29],
//		arr[i].filler[30],arr[i].filler[31],arr[i].filler[32],arr[i].filler[33],arr[i].filler[34],arr[i].filler[35],arr[i].filler[36],arr[i].filler[37],arr[i].filler[38],arr[i].filler[39],
//		arr[i].filler[40],arr[i].filler[41],arr[i].filler[42],arr[i].filler[43],arr[i].filler[44],arr[i].filler[45],arr[i].filler[46],arr[i].filler[47],arr[i].filler[48],arr[i].filler[49],
//		arr[i].filler[50],arr[i].filler[51],arr[i].filler[52],arr[i].filler[53],arr[i].filler[54],arr[i].filler[55]);
		i++;
	}
	printf("%d",i);
	fclose(fp);
    if (line)
        free(line);
    qsort(arr, SOME_BIG_THING, sizeof(arr[0]), comp); 
    for(int i=0;i<SOME_BIG_THING;i++)
    {
    	printf("%d\n", arr[i].id);
    }
    exit(EXIT_SUCCESS);
}

ssize_t getdelim(char **linep, size_t *n, int delim, FILE *fp){
    int ch;
    size_t i = 0;
    if(!linep || !n || !fp){
        errno = EINVAL;
        return -1;
    }
    if(*linep == NULL){
        if(NULL==(*linep = malloc(*n=128))){
            *n = 0;
            errno = ENOMEM;
            return -1;
        }
    }
    while((ch = fgetc(fp)) != EOF){
        if(i + 1 >= *n){
            char *temp = realloc(*linep, *n + 128);
            if(!temp){
                errno = ENOMEM;
                return -1;
            }
            *n += 128;
            *linep = temp;
        }
        (*linep)[i++] = ch;
        if(ch == delim)
            break;
    }
    (*linep)[i] = '\0';
    return !i && ch == EOF ? -1 : i;
}
ssize_t getline(char **linep, size_t *n, FILE *fp){
    return getdelim(linep, n, '\n', fp);
}
